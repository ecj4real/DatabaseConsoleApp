﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DatabaseConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            DBConnect conn = new DBConnect();

            int input= 0;
            do
            {
                Console.WriteLine("\n\t1: View the Society Table\n\t2: Insert into the Society Table"
                    +"\n\t3: Update the Society Table\n\t4: Delete from Members Table"
                    +"\n\t5: Exit\n");
                Console.Write("Enter choice: ");
                string choice = Console.ReadLine();
                if (Regex.IsMatch(choice, "[1-5]"))
                {
                    input = Convert.ToInt16(choice);

                    switch (input)
                    {
                        case 1:
                            conn.selectTable();
                            break;
                        case 2:
                            Console.Write("Enter the Name of the Society: ");
                            string nameOfSociety = Console.ReadLine();
                            if (conn.insertTable(nameOfSociety))
                            {
                                Console.WriteLine("Insert Successful");
                            }
                            else
                            {
                                Console.WriteLine("Insert Failed");
                            }
                            break;
                        case 3:
                            Console.Write("Enter the Name of the Society: ");
                            string name = Console.ReadLine();
                            Console.Write("Enter the New Name: ");
                            string replacement = Console.ReadLine();

                            if (conn.updateTable(name,replacement))
                            {
                                Console.WriteLine("Update Successful");
                            }
                            else
                            {
                                Console.WriteLine("Update Failed");
                            }
                            break;
                        case 4:
                            Console.Write("Enter the ID of Member: ");
                            try
                            {
                                int id = Convert.ToInt16(Console.ReadLine());
                                if (conn.deleteRecord(id))
                                {
                                    Console.WriteLine("Delete Successful");
                                }
                                else
                                {
                                    Console.WriteLine("Delete Failed");
                                }
                            }
                            catch (Exception ex)
                            {

                                Console.WriteLine(ex.Message);
                            }
                            break;
                        case 5:
                            Console.WriteLine("Thank You!!!");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Invalid input");
                    continue;
                }
            } while (input != 5);

            Console.Write("\n\nPress any key to continue . . . ");
            Console.ReadKey();
        }
    }
}
