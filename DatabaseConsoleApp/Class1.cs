﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace DatabaseConsoleApp
{
    class DBConnect
    {
        private MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string port;
        private string password;

        public DBConnect()
        {
            Initialize();
        }

        private void Initialize()
        {
            server = "127.0.0.1";
            database = "churchdatabase";
            uid = "root";
            port = "1234";
            password = "password";

            string connectionString = "SERVER=" + server + ";" + "DATABASE=" + database + ";" + "UID=" + uid + ";" +
                "PORT=" + port + ";" + "PASSWORD=" + password + ";";
            connection = new MySqlConnection(connectionString);
        }

        //open connection to database
        private bool openConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            
            return false;
        }

        //close connection to database
        private bool closeConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch(MySqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return false;
        }

        public void selectTable()
        {
            openConnection();

            string query = "Select * from society";
            MySqlCommand command = new MySqlCommand(query, connection);

            MySqlDataReader output = command.ExecuteReader();
            Console.WriteLine("\n\tID\tName of Society");
            Console.WriteLine("\t__\t_______________");
            while (output.Read())
            {
                Console.Write("\t" + output.GetString("id"));
                Console.Write("\t" + output.GetString("name"));
                Console.WriteLine();
            }
           
            closeConnection();

        }

        public bool insertTable(string name)
        {
            string query = "insert into society (name) values (@name);";
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandText = query;
            command.Parameters.AddWithValue("@name", name);

            openConnection();
            int exec = command.ExecuteNonQuery();
            closeConnection();

            return exec == 1;
        }

        public bool updateTable(string name, string replace)
        {
            string query = "update society set name=@replacement where name=@societyName;";

            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandText = query;
            command.Parameters.AddWithValue("@societyName", name);
            command.Parameters.AddWithValue("@replacement", replace);

            openConnection();
            int exec = command.ExecuteNonQuery();
            closeConnection();

            return exec == 1;
        }

        public bool deleteRecord(int id)
        {
            string query = "delete from members where id = @id;";

            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandText = query;
            command.Parameters.AddWithValue("@id", id);

            openConnection();
            int exec = command.ExecuteNonQuery();
            closeConnection();

            return exec == 1;
        }
    }
}
